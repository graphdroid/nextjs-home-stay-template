import Navbar from "./ui/components/navbar/Navbar";
import Hero from "./ui/sections/hero/page";
import About from "./ui/sections/about/page";
import Explore from "./ui/sections/explore/page";
import Service from "./ui/sections/service/page";
import Footer from "./ui/sections/footer/page";

export default function Home() {
  return (
    <>
    <Navbar />
    <Hero />
    <About />
    <Explore />
    <Service />
    <Footer />
    </>
  );
}
