import Link from 'next/link'
import Image from 'next/image'

import homeIcon from '../../../../public/icon/home-icon.svg'
import aboutIcon from '../../../../public/icon/about-icon.svg'
import servicesIcon from '../../../../public/icon/services-icon.svg'

type navLinkProps = {
    device: string
}

const links = [
    {name: 'Home', href: '/#home-section', icon: homeIcon },
    {name: 'About', href: '/#about-section', icon: aboutIcon },
    {name: 'Services', href: '/#services-section', icon: servicesIcon }
]

const NavLinks = (props: navLinkProps) => {
    return (
        <div className={props.device} >
            {links.map((link) => {
                return (
                    <Link
                        key={link.name}
                        href={link.href}
                        className='navlinks' >
                            <Image src={link.icon} alt='icon' />
                            {link.name}
                    </Link>
                )
            })}
        </div>
    )
}

export default NavLinks