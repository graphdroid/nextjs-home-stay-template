'use client'
import { useState } from 'react'
import Image from 'next/image'

import mobileLogo from '../../../../public/logo/mobile-logo.svg'

import hamburger from '../../../../public/icon/hamburger.svg'
import closeBtn from '../../../../public/icon/close-icon.svg'

import NavLinks from './NavLinks'

const Navbar = () => {
    const [collapsed, setCollapsed] = useState<boolean>(true)

    // const [isHomeHovered, setIsHomeHovered] = useState<boolean>(false)
    // const [isAboutHovered, setIsAboutHovered] = useState<boolean>(false)
    // const [isServicesHovered, setIsServicesHovered] = useState<boolean>(false)
    
    return (
        <div className="h-28">
            <NavLinks device="hidden md:flex flex-row flex-wrap justify-center gap-9 h-14" />
            {collapsed ? 
                <Image src={hamburger} alt="hamburger-menu" className="hamburger-menu absolute inset-6 cursor-pointer md:hidden" onClick={() => setCollapsed(false)} /> : 
                <div className="bg-secondary text-white">
                    <Image src={closeBtn} alt="close-btn" className="close-btn md:hidden" onClick={() => setCollapsed(true)} />
                    <Image src={mobileLogo} alt="Logo" id="mobile-logo" className="h-0 md:hidden" />
                    
                    <NavLinks device="flex flex-col flex-wrap justify-center gap-12 md:hidden" />
                    {/* <ul>
                        {isHomeHovered ? 
                            <li onMouseEnter={() => setIsHomeHovered(true)} onMouseLeave={() => setIsHomeHovered(false)} className="nav-links" ><a href="#hero-section">Home</a></li> :
                            <li onMouseEnter={() => setIsHomeHovered(true)} onMouseLeave={() => setIsHomeHovered(false)} className="nav-links"><a href="#hero-section"><Image src={homeIcon} alt="home-icon" className="mobile-nav-icon" />Home</a></li>
                        }
                        {isAboutHovered ? 
                            <li onMouseEnter={() => setIsAboutHovered(true)} onMouseLeave={() => setIsAboutHovered(false)} className="nav-links"><a href="#hero-section">Abuot</a></li> :
                            <li onMouseEnter={() => setIsAboutHovered(true)} onMouseLeave={() => setIsAboutHovered(false)} className="nav-links"><a href="#hero-section"><Image src={aboutIcon} alt="about-icon" className="mobile-nav-icon" />About</a></li>
                        }
                        {isServicesHovered ? 
                            <li onMouseEnter={() => setIsServicesHovered(true)} onMouseLeave={() => setIsServicesHovered(false)} className="nav-links"><a href="#hero-section">Services</a></li> :
                            <li onMouseEnter={() => setIsServicesHovered(true)} onMouseLeave={() => setIsServicesHovered(false)} className="nav-links"><a href="#hero-section"><Image src={servicesIcon} alt="services-icon" className="mobile-nav-icon" />Services</a></li>
                        }
                    </ul> */}
                    {/* <Button className="rectangle-btn btn-medium" value="Register"/> */}
                </div>
            }
    </div>
    )
}

export default Navbar