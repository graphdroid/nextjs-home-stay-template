import Image from "next/image"

import { createClient } from "@/utils/supabase/server"
import { cookies } from "next/headers"

const Cards = async () => {
    const cookieStore = cookies()
    const supabase = createClient(cookieStore)
    const {data: cards} = await supabase.from('card_view').select('*')
    return (
        <div className="ml-[150px] min-h-[625px] flex gap-[25px] overflow-x-auto card-container">
            {cards?.map((card) => {
                return (
                    <div key={card.id} className="min-w-[374px] h-[575px] p-[25px] shadow-xl" >
                        <Image src={card.img_path} alt={card.img_path} width={100} height={100} className="w-72 h-60 md:w-96 md:h-60" />
                        <div className="flex items-center justify-between mt-[25px]">
                            <h2>{card.name}</h2>
                            <h5>{card.price}/Night</h5>
                        </div>
                        <p className="text-justify mt-4 line-clamp-6">{card.about}</p>
                    </div>
                )
            })}
        </div>
    )
}

export default Cards