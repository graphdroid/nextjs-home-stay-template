import Cards from "../../components/Cards"

const Explore = async () => {
    return (
        <div className='relative top-24 lg:pb-0'>
            <section id="explore-section" className='relative'>
                <h1 className="pt-9 pb-[75px] text-primary">Explore</h1>
                <Cards />
            </section>
        </div>
    )
}

export default Explore