import Image from 'next/image'

import serviceImg from '../../../../public/illustrations/services-img.svg'

const services = [
    { id: 1, service: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua" },
    { id: 2, service: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua" },
    { id: 3, service: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua" },
    { id: 4, service: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua" }
]

const Service = () => {
    return (
        <div className="bg-primary text-white relative mt-24">
        <section id="services-section" className="pt-14">
            <h1>Services</h1>
            <div className="section-container flex-col">
                <div className='relative paragraph'>
                    <ul className='pt-12 lg:-top-12 lg:pt-0'>
                        {services.map((data) => {
                        return (
                            <li key={data.id} className="mb-4 pl-2 list-image-list">{data.service}</li>
                            )
                        })}
                    </ul>
                </div>
                <div>
                    <Image src={serviceImg} alt="services-img" id="services-img"/>
                </div>
            </div>
        </section>
        </div>
    )
}

export default Service