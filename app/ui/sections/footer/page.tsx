import Image from 'next/image'

import FacebookLogo from '../../../../public/logo/facebook-logo.svg'
import InstagramLogo from '../../../../public/logo/instagram-logo.svg'
import TwitterLogo from '../../../../public/logo/twitter-logo.svg'

import FacebookLogoHover from '../../../../public/logo/facebook-logo.svg'
import InstagramLogoHover from '../../../../public/logo/instagram-logo.svg'
import TwitterLogoHover from '../../../../public/logo/twitter-logo.svg'

const logo = [
    { id: 'facebook-logo', imageUrl: FacebookLogo, hoverImgUrl: FacebookLogoHover },
    { id: 'instagram-logo', imageUrl: InstagramLogo, hoverImgUrl: InstagramLogoHover },
    { id: 'twitter-logo', imageUrl: TwitterLogo, hoverImgUrl: TwitterLogoHover }
]

const Footer = () => {
    return (
        <div className="bg-black text-white relative">
        <section id="footer-section" className="pt-8 -mt-20">
            <h1>Footer</h1>
            <div className="flex justify-center">
                <div>
                    <ul className="flex flex-row justify-center space-x-8">
                        {logo.map((data) => {
                            return (
                                <li key={data.id}><Image src={data.imageUrl} alt={data.id} /></li>
                            )
                        })}
                    </ul>
                <p>Copyright &copy; 2023 - 2024</p>
                </div>
            </div>
        </section>
        </div>
    )
}

export default Footer