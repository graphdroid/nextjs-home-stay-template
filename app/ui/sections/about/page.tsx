import Image from 'next/image'

import aboutImg from '../../../../public/illustrations/about-img.svg'
import backgroundImg from '../../../../public/backgrounds/background.svg'

const About = () => {
    return (
        <div className='bg-primary relative top-24 lg:pb-0'>
            <Image src={backgroundImg} alt="about-background image" className='absolute -top-20' />
            <section id="about-section" className='relative text-white'>
                <h1>About Us</h1>
                <div className="section-container flex-col-reverse relative top-36 lg:top-0 paragraph">
                    <ul className='relative -top-56 lg:-top-16'>
                        <li className="mb-6">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In tellus integer feugiat scelerisque varius morbi enim nunc. Scelerisque purus semper eget duis at. At tempor commodo ullamcorper a. Non enim praesent elementum facilisis leo vel fringilla est ullamcorper. Arcu cursus euismod quis viverra nibh cras. Mattis aliquam faucibus purus in massa tempor nec feugiat. Arcu cursus euismod quis viverra nibh cras. Risus feugiat in ante metus dictum at tempor commodo.</li>
                        <li className="mb-6">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In tellus integer feugiat scelerisque varius morbi enim nunc. Scelerisque purus semper eget duis at. At tempor commodo ullamcorper enim.</li>
                        <li><i className="flex justify-end">- Home Stay</i></li>
                    </ul>
                    <Image src={aboutImg} alt="about-illustrations" id='about-img' />
                </div>
            </section>
        </div>
    )
}

export default About