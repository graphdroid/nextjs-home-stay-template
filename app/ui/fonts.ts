import { Ubuntu, Nunito } from "next/font/google";

export const ubuntu = Ubuntu({
    weight: ['400', '700'], 
    variable: '--primary-font',
    subsets: ['latin']
})

export const nunito = Nunito({
    weight: ['400', '500'], 
    variable: '--secondary-font',
    subsets: ['latin']
})