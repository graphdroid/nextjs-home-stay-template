import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        ubuntu: ['var(--primary-font)'],
        nunito: ['var(--secondary-font)']
      },
      fontSize: {
        'text-1sm': '10px',
        'text-2sm': '8px'
      },
      colors: {
        'primary': '#ba8c63',
        'secondary': '#a27248',
        'complementary': '#63918a'
      },
      listStyleImage: {
        'list': 'url("../public/icon/check-icon-desktop.svg")'
      }
    },
  },
  plugins: [],
};
export default config;
